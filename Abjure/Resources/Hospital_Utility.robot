*** Settings ***
# Documentation     An example resource file
Library           SeleniumLibrary
Resource    PageObjects/Hospital/Hospital_Login.robot

*** Variables ***


*** Keywords ***
Opening browser
    Open browser    https://abh.webmobtech.biz/    chrome
    Maximize browser window

Closing browser
    Close All Browsers

Login
    [Arguments]    ${Username}    ${password}
    Enter Username        ${Username}
    Enter Password        ${password}
    Click on Login


Logout
    Wait Until Element Is Visible    //*[@id="root"]/section/div[1]/section/header/ul/li[7]/div
    Mouse Over    //*[@id="root"]/section/div[1]/section/header/ul/li[7]/div
    Sleep    2
    Set Selenium Timeout    10
    Wait Until Element Is Visible    //ul[contains(@id,'tmp_key-6-popup')]
    # Set Focus To Element    //ul[contains(@id,'tmp_key-6-popup')]
    Click Element    //li[contains(@data-menu-id,'tmp_key-tmp_key-6-1')]
    Sleep    5

Clear Text
  [Arguments]  ${inputField}
  press keys  ${inputField}  CTRL+a+BACKSPACE

