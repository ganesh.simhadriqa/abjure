*** Settings ***
Library    SeleniumLibrary 
Resource    ../../Hospital_Utility.robot


*** Variables ***
# Common
${Jobs_Templates}            //a[@href='/template']
${Add_Template}            //*[@id="root"]/section/div[3]/div/main/div/div/div/div[1]/div[3]/button
${Add_Template_Text_Verification}            //*[@id="root"]/section/div[3]/div/main/div/div[1]/div/div
${Teplate_Name}            //input[@id='template_name']
${JOb_Title}            //input[@id='name']
${Job_Type}            //span[@class='ant-select-selection-item']
${Slots_Available}            //input[@id='slots']
${Min_EXp}            //input[@id='min_experience']
${Max_Exp}            //input[@id='max_experience']
${Cancel_Button}            //button[@class='ant-btn ant-btn-text ant-btn-lg']
${Save}            //button[@type='submit']
# Hospitalist
${Family_Practice_True}            //*[@id='family_practice_physicians_apply']/label[1]/span[1]/input
${Family_Practice_False}            //*[@id='family_practice_physicians_apply']/label[2]/span[1]/input
${Avg_Hospital_Census}            //input[@id='hospital_census']
${Expected_Patient_Load}            //input[@id='expected_patient_load']
${Avg_Admissions_Day}            //input[@id='avg_admissions_per_provider_by_day']
${Avg_ER_Admissions_Night}            //input[@id='avg_er_admissions_per_provider_by_night']
${Hospitalist_Day}            //input[@id='hospitalist_in_day']
${Hospitalist_Night}            //input[@id='hospitalist_in_night']
${Mid_Levels_Day}            //input[@id='mid_levels_in_day']
${Mid_Levels_Night}            //input[@id='mid_levels_in_night']
${Swing_Shift_Available_True}            //*[@id='swing_shift_available']/label[1]/span[1]/input
${Swing_Shift_Available_False}            //*[@id='swing_shift_available']/label[2]/span[1]/input
${Backup_Plan_No_more_Admissions_Allowed}            //*[@id='backup_plan']/div/div[1]/label/span[1]/input
${Backup_Provider_Available}            //*[@id='backup_plan']/div/div[2]/label/span[1]/input
${Can_Increase_Patient_Load_For_Additional_Compensation}            //*[@id='backup_plan']/div/div[3]/label/span[1]/input
${To_Exceed_Agreed_Patient_Load_For_Additional_Compensation}            //*[@id='backup_plan']/div/div[4]/label/span[1]/input
${To_Exceed_Agreed_Patient_Load_Without_Additional_Compensation}            //*[@id='backup_plan']/div/div[5]/label/span[1]/input
${Mid_Level_Available}            //*[@id='mid_level_available']/label[1]/span[1]/input
${Mid_Level_Not_Available}            //*[@id='mid_level_available']/label[2]/span[1]/input
${Mid_Level_Available_For_DayShift}            //*[@id="mid_level_available_for_shift"]/label[1]/span[1]/input
${Mid_Level_Available_For_Night Shift}            //*[@id="mid_level_available_for_shift"]/label[2]/span[1]/input
${Mid_Level_Available_For_24hr_Shift}            //*[@id="mid_level_available_for_shift"]/label[3]/span[1]/input
${ER_Physician_Runs_Code}            //*[@id="runs_code"]/label[1]/span[1]/input
${Intensivist_Runs_Code}            //*[@id="runs_code"]/label[2]/span[1]/input
${Hospitalist_Runs_Code}            //*[@id="runs_code"]/label[3]/span[1]/input
${Respiratory_Therapist_Available}            //*[@id="respiratory_therapist"]/label[1]/span[1]/input
${Respiratory_Therapist_Not_Available}            //*[@id="respiratory_therapist"]/label[2]/span[1]/input
${Respiratory_Therapist_Allowed_To_Intubate}            //*[@id="respiratory_therapist_allowed_intubate"]/label[1]/span[1]/input
${Respiratory_Therapist_Not_Allowed_To_Intubate}            //*[@id="respiratory_therapist_allowed_intubate"]/label[2]/span[1]/input
${Procedures_Required_Central_Line}            //*[@id="procedures_required"]/label[1]/span[1]/input
${Procedures_Required_Arterial_Line}            //*[@id="procedures_required"]/label[2]/span[1]/input
${Procedures_Required_Intubation}            //*[@id="procedures_required"]/label[3]/span[1]/input
${Procedures_Required_Chest_Tube}            //*[@id="procedures_required"]/label[4]/span[1]/input
${Procedures_Required_Lumbar_Puncture}            //*[@id="procedures_required"]/label[5]/span[1]/input
${Relationship_With_Candidate_Short-Term}            //*[@id="contract_period_type"]/label[1]/span[1]/input
${Relationship_With_Candidate_Long-Term}            //*[@id="contract_period_type"]/label[2]/span[1]/input
${Flexible_With_Shift_Scheduling_Yes}            //*[@id="flexible_shift_scheduling"]/label[1]/span[1]/input
${Flexible_With_Shift_Scheduling_No}            //*[@id="flexible_shift_scheduling"]/label[2]/span[1]/input
${Job_Description}            //*[@id="description"]
# General Surgeon
${Sellecting_General_Surgeon}            (//div[@class='ant-select-item-option-content'])[1]
${Outpatient_Clinic_Responsibilities_Required}            //*[@id="is_verified_clinic"]/label[1]/span[1]/input
${Outpatient_Clinic_Responsibilities_NotRequired}            //*[@id="is_verified_clinic"]/label[2]/span[1]/input
${Number_Of_Clinic_Days_Per_Week}            //input[@id='clinic_days_per_week']
${Average_Number_Of_Clinic_Patients_Per_Day}            //input[@id='avg_no_of_patients_per_day']
${EMR_Used_In_Clinic}            //input[@id='emr_used_in_clinic']
${Average Number Surgeries Per OR Day}            //input[@id='avg_no_of_surgeries_per_or_day']
${Anesthesia_Care}                //input[@id='anesthesia_care']
${Anesthesia_Care_CRNA}            //div[@title='CRNA']
${Anesthesia_Care_Anesthesiologist}            //div[@title='Anesthesiologist']
${Anesthesia_Care_Both}            //div[@title='Both']
${Upper_And_Lower_Endoscopy_Required_Yes}            //*[@id="upper_lower_endoscopy"]/label[1]/span[1]/input
${Upper_And_Lower_Endoscopy_Required_No}            //*[@id="upper_lower_endoscopy"]/label[2]/span[1]/input
${Trauma_Level_I}            //*[@id="trauma_center_level"]/label[1]/span[1]/input
${Trauma_Level_II}            //*[@id="trauma_center_level"]/label[2]/span[1]/input
${Trauma_Level_III}            //*[@id="trauma_center_level"]/label[3]/span[1]/input
${Trauma_Level_IV}            //*[@id="trauma_center_level"]/label[4]/span[1]/input
${Trauma_Level_V}            //*[@id="trauma_center_level"]/label[5]/span[1]/input
${Average_Number_Of_Trauma_Patients_Per_Month}            //input[@id='avg_no_of_trauma_patients_per_month']
${C-Section_Required_Yes}            //*[@id="c_section"]/label[1]/span[1]/input
${C-Section_Required_No}            //*[@id="c_section"]/label[2]/span[1]/input
${Dialysis_Access_Required_Yes}            //*[@id="dialysis_access"]/label[1]/span[1]/input
${Dialysis_Access_Required_No}            //*[@id="dialysis_access"]/label[2]/span[1]/input
${Dialysis_Access_Catheter}            //input[@value='CATHETER_ONLY']
${Dialysis_Access_AV_Graft/Fistula}            //input[@value='AV_GRAFT/FISTULA']
${Dialysis_Access_Both}            //input[@value='BOTH']
# Add Job
${Jobs}            //a[@href='/jobs/active']
${Add_Job_Button}            //*[@id="root"]/section/div[3]/div/main/div/div/div/div[1]/div/button
${Add_Job_Nav_Verify}            //div[@class='fs-21 text-bold']
${Dashboard_Add_Job}            //a[@href='/jobs/add']
${Click_On_Template_Drop_Down_Field}            //input[@id='template']
${Select_Template}            (//div[contains(@class,'ant-select-item ant-select-item-option')])[2]
# Schedule Job
${Start_Date}            //input[@id='job_schedule_0_range_date']
${End_Date}            //input[@placeholder='End date']
${Shift}            //div[@class='ant-select-selector']
${List_Box}            /html/body/div[3]/div/div
${Day_Shift}            (//div[@class='ant-select-item-option-content'])[1]
${Night_Shift}            (//div[@class='ant-select-item-option-content'])[2]
${24hrs_Shift}            (//div[@class='ant-select-item-option-content'])[3]
${Start_Time}            //input[@id='job_schedule_0_start_time']
${End_Time}            //input[@id='job_schedule_0_end_time']
${Tomorrow_Date}            (//td[@class='ant-picker-cell ant-picker-cell-in-view'])[1]
# Payment
${Hourly}            //input[@value='HOURLY']
${Hourly_Payment}            (//input[@class='ant-input-number-input'])[1]
${Fixed}            //input[@value='FIXED']
${Fixed_Payment}            (//input[@class='ant-input-number-input'])[2]
${Ok}            //button[@type='button']
# Edit Job
${Edit_Job_Button}            //*[@id="rc-tabs-0-panel-/jobs/active"]/div/div/div[1]/div/div/div[1]/div[1]/div[2]/button/span
# Job Templates
${Use_Template}            (//span[text()='Use Template'])[1]







*** Keywords ***
Click Dashboard Add Job
    Wait Until Element Is Visible    ${Dashboard_Add_Job}
    Click Link    ${Dashboard_Add_Job}
    Sleep    2
    Element Should Contain    ${Add_Job_Nav_Verify}    Add Job

Go to Jobs Templates
    Wait Until Element Is Visible    ${Jobs_Templates}
    Click Link    ${Jobs_Templates}
    Sleep    3
    Title Should Be    Template

Click Add Template Button
    Wait Until Element Is Visible    ${Add_Template}
    Click Button    ${Add_Template}
    Sleep    3
    Element Should Contain    ${Add_Template_Text_Verification}    Add Template

Fill General Surgeon Template 
    Input Text    ${Teplate_Name}    text
    Input Text    ${JOb_Title}    text
    Click Element    ${Job_Type}
    Sleep    3
    Click Element    ${Sellecting_General_Surgeon}
    Clear Text    ${Slots_Available}
    Input Text    ${Slots_Available}    8
    Clear Text    ${Min_EXp}
    Input Text    ${Min_EXp}    1
    Clear Text    ${Max_Exp}
    Input Text    ${Max_Exp}    8
    Click Element    ${Outpatient_Clinic_Responsibilities_NotRequired}
    Input Text    ${Average Number Surgeries Per OR Day}    3
    Click Element    ${Anesthesia_Care}
    Sleep    2
    Click Element    ${Anesthesia_Care_Both}
    Click Element    ${Upper_And_Lower_Endoscopy_Required_No}
    Click Element    ${Trauma_Level_IV}
    Input Text    ${Average_Number_Of_Trauma_Patients_Per_Month}    99
    Click Element    ${C-Section_Required_No}
    Click Element    ${Dialysis_Access_Required_No}
    Input Text    ${Job_Description}    Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test Ganesh test 
    Sleep    5
    # Click Element    ${Cancel_Button}
    Click Element    ${Save}
    Sleep    7


Fill Hospitalist Template
    Input Text    ${Teplate_Name}    SR.Hospitalist | 5-12 Exp
    Input Text    ${JOb_Title}    SR.Hospitalist
    Click Element    ${Job_Type}
    Clear Text    ${Slots_Available}
    Input Text    ${Slots_Available}    8
    Clear Text    ${Min_EXp}
    Input Text    ${Min_EXp}    5
    Clear Text    ${Max_Exp}
    Input Text    ${Max_Exp}    12
    Click Element    ${Family_Practice_True}
    Input Text    ${Avg_Hospital_Census}    9
    Input Text    ${Expected_Patient_Load}    10
    Input Text    ${Avg_Admissions_Day}    11
    Input Text    ${Avg_ER_Admissions_Night}    12
    Input Text    ${Hospitalist_Day}    13
    Input Text    ${Hospitalist_Night}    14
    Input Text    ${Mid_Levels_Day}    15
    Input Text    ${Mid_Levels_Night}    0
    Click Element    ${Swing_Shift_Available_True}
    Click Element    ${Backup_Provider_Available}
    Click Element    ${Mid_Level_Not_Available}
    Click Element    ${Intensivist_Runs_Code}
    Click Element    ${Respiratory_Therapist_Not_Available}
    Click Element    ${Procedures_Required_Central_Line}
    Click Element    ${Procedures_Required_Arterial_Line}
    Click Element    ${Procedures_Required_Intubation}
    Click Element    ${Procedures_Required_Chest_Tube}
    Click Element    ${Procedures_Required_Lumbar_Puncture}
    Click Element    ${Relationship_With_Candidate_Long-Term}
    Click Element    ${Flexible_With_Shift_Scheduling_Yes}
    Input Text    ${Job_Description}    Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist Ganesh Test Hospitalist 
    Sleep    4
    # Click Button    ${Cancel_Button}
    Click Button    ${Save}
    Sleep    5


Go to Jobs
    Wait Until Element Is Visible    ${Jobs}
    Click Link    ${Jobs}
    Sleep    3
    Title Should Be    Jobs

Click Add Job Button
    Wait Until Element Is Visible    ${Add_Job_Button}
    Click Button    ${Add_Job_Button}
    Sleep    3
    Element Should Contain    ${Add_Job_Nav_Verify}    Add Job


Add General Surgeon
    Input Text    ${JOb_Title}    Sr Surgeon
    Click Element    ${Job_Type}
    Sleep    3
    Click Element    ${Sellecting_General_Surgeon}
    Clear Text    ${Slots_Available}
    Input Text    ${Slots_Available}    8
    Clear Text    ${Min_EXp}
    Input Text    ${Min_EXp}    1
    Clear Text    ${Max_Exp}
    Input Text    ${Max_Exp}    8
    Click Element    ${Outpatient_Clinic_Responsibilities_NotRequired}
    Input Text    ${Average Number Surgeries Per OR Day}    3
    Click Element    ${Anesthesia_Care}
    Sleep    2
    Click Element    ${Anesthesia_Care_Both}
    Click Element    ${Upper_And_Lower_Endoscopy_Required_No}
    Click Element    ${Trauma_Level_IV}
    Input Text    ${Average_Number_Of_Trauma_Patients_Per_Month}    99
    Click Element    ${C-Section_Required_No}
    Click Element    ${Dialysis_Access_Required_No}
    Sleep    5
    # Click Element    ${Cancel_Button}
    Click Element    ${Save}
    Sleep    7
    

Add Hospitalist
    Input Text    ${JOb_Title}    SR.Hospitalist-Don't use this job2
    Click Element    ${Job_Type}
    Clear Text    ${Slots_Available}
    Input Text    ${Slots_Available}    8
    Clear Text    ${Min_EXp}
    Input Text    ${Min_EXp}    5
    Clear Text    ${Max_Exp}
    Input Text    ${Max_Exp}    12
    Click Element    ${Family_Practice_True}
    Input Text    ${Avg_Hospital_Census}    9
    Input Text    ${Expected_Patient_Load}    10
    Input Text    ${Avg_Admissions_Day}    11
    Input Text    ${Avg_ER_Admissions_Night}    12
    Input Text    ${Hospitalist_Day}    13
    Input Text    ${Hospitalist_Night}    14
    Input Text    ${Mid_Levels_Day}    15
    Input Text    ${Mid_Levels_Night}    0
    Click Element    ${Swing_Shift_Available_True}
    Click Element    ${Backup_Provider_Available}
    Click Element    ${Mid_Level_Not_Available}
    Click Element    ${Intensivist_Runs_Code}
    Click Element    ${Respiratory_Therapist_Not_Available}
    Click Element    ${Procedures_Required_Central_Line}
    Click Element    ${Procedures_Required_Arterial_Line}
    Click Element    ${Procedures_Required_Intubation}
    Click Element    ${Procedures_Required_Chest_Tube}
    Click Element    ${Procedures_Required_Lumbar_Puncture}
    Click Element    ${Relationship_With_Candidate_Long-Term}
    Click Element    ${Flexible_With_Shift_Scheduling_Yes}
    Sleep    4
    # Click Button    ${Cancel_Button}
    Click Button    ${Save}
    Sleep    5

Schedule Job
    Wait Until Element Is Visible    ${Start_Date}
    Click Element    ${Start_Date}
    Click Element    ${Tomorrow_Date}
    Click Element    ${Tomorrow_Date}
    Click Element    ${Shift}
    Wait Until Element Is Visible    ${Night_Shift}
    Click Element    ${Night_Shift}
    Click Element    ${Start_Time}
    Clear Text    ${Start_Time}
    Input Text    ${Start_Time}    07:00
    Press Keys    ${Start_Time}    RETURN
    Click Element    ${End_Time}
    Clear Text    ${End_Time}
    Input Text    ${End_Time}    08:00
    Press Keys    ${End_Time}    RETURN
    Save

Click Edit Job Button
    Click Element    ${Edit_Job_Button}

Save
    Wait Until Element Is Visible    ${Save}
    Click Element    ${Save}

Add Payment Type as Hourly
    Wait Until Element Is Visible    ${Hourly_Payment}
    Click Element    ${Hourly}
    Clear Text    ${Hourly_Payment}
    Input Text    ${Hourly_Payment}    46
    Save

Add Payment Type as Fixed
    Wait Until Element Is Visible    ${Fixed_Payment}
    Click Element    ${Fixed}
    Clear Text    ${Fixed_Payment}
    Input Text    ${Fixed_Payment}    250
    Save

Add Job description
    Wait Until Element Is Visible    ${Job_Description}
    Input Text    ${Job_Description}    Don't use this job this is for Automation testing.
    Save
    

Click On Use Template Link
    Wait Until Element Is Visible    ${Use_Template}
    Click Element    ${Use_Template}

Select Template
    Set Selenium Timeout    10
    Click Element    ${Click_On_Template_Drop_Down_Field}
    Wait Until Element Is Visible    ${Select_Template}
    Click Element    ${Select_Template}
    


