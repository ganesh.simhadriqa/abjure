*** Settings ***
Library    SeleniumLibrary
Resource    ../../Hospital_Utility.robot

*** Variables ***
${Username}    id:email
${pswd}    id:password
${Submit}        //button[@type='submit']

*** Keywords ***
Enter Username
    Wait Until Element Is Visible    ${Username}
    [Arguments]                ${email}
    Clear Text    ${Username}
    Input Text    ${Username}    ${email}
    
Enter Password
    [Arguments]                  ${password}  
    Clear Text    ${pswd} 
    Input Text    ${pswd}     ${password}

Click on Login
    Click Button    ${Submit}