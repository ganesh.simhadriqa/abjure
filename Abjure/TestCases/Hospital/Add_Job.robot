*** Settings ***
Library    SeleniumLibrary
Suite Setup    Opening browser
Resource    ../../Resources/Hospital_Utility.robot
Resource    ../../Resources/PageObjects/Hospital/AddJob_AddTemplate.robot
Suite Teardown    Closing browser
Test Setup    Login    Abjurephysicianqa@mailinator.com    Test@123
Test Teardown    Logout

*** Test Cases ***
Add Job For General Surgeon
    Go to Jobs
    Click Add Job Button
    Add General Surgeon
    Schedule Job
    Add Payment Type as Fixed
    Add Job description
    Sleep    3

Add Job For Hospitalist
    Go to Jobs
    Click Add Job Button
    Add Hospitalist
    Schedule Job
    Add Payment Type as Hourly
    Add Job description
    Sleep    4

Add Job For General Surgeon From Dashboard
    Click Dashboard Add Job
    Add General Surgeon
    Schedule Job
    Add Payment Type as Hourly
    Add Job description
    Sleep    2

Add Job For Hospitalist From Dashboard
    Click Dashboard Add Job
    Add Hospitalist
    Schedule Job
    Add Payment Type as Fixed
    Add Job description
    Sleep    2

Add Job By Selecting Template from Dropdown
    Click Dashboard Add Job
    Select Template
    Sleep    6
    # Save
    # Schedule Job
    # Add Payment Type as Fixed
    # Save

Add Job By Clicking on Use Template From Template Screen
    Go to Jobs Templates
    Click On Use Template Link
    Sleep    6
    # Save
    # Schedule Job
    # Add Payment Type as Hourly
    # Save

Verifying Job card details after adding job
Verify Job Details Inner Page and Application
Verify Job Update Functionality
Verify Inactive Job Flow
Verify Mark as Filled Functionality

