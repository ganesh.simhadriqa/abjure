*** Settings ***
Library    SeleniumLibrary
Resource    ../../Resources/PageObjects/Hospital/AddJob_AddTemplate.robot
Suite Setup    Opening browser
# Suite Teardown    Closing browser
Test Setup    Login    Abjurephysicianqa@mailinator.com    Test@123
Test Teardown    Logout

*** Test Cases ***
Add General Surgeon Template
    Go to Jobs Templates
    Click Add Template Button
    Fill General Surgeon Template


Add Hospitalist Template
    Go to Jobs Templates
    Click Add Template Button
    Fill Hospitalist Template