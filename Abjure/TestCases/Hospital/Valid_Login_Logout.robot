*** Settings ***
Library    SeleniumLibrary
Suite Setup    Opening browser
Suite Teardown    Closing browser
Resource    ../../Resources/Hospital_Utility.robot

*** Test Cases ***
Login & Logout
    Login    Abjurephysicianqa@mailinator.com    Test@123
    Sleep    6
    Title Should Be    Dashboard
    Sleep    3
    Logout
    Title Should Be    Login
