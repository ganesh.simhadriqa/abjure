*** Settings ***
Library    SeleniumLibrary
Suite Setup    Opening browser
Suite Teardown    Closing browser
Resource    ../../Resources/Physician_Utility.robot
Resource    ../../Resources/PageObjects/Physician/Physician_Login.robot

*** Test Cases ***
Case1
    [Template]    Both Invalid
    wmtgtt@gmail.com    test@123
    

Valid username and invalid pswd
    [Template]    Both Invalid
    Abjurephysicianqa@mailinator.com    test@123

Invalid username and Valid pswd
    [Template]    Both Invalid
    wmtgtut@gmail.com    Test@123

Case4
    [Template]    Both Empty
    ${EMPTY}    ${EMPTY}


Case5
    [Template]    Empty username and Valid pswd
    ${EMPTY}    Test@123

case6
    [Template]    Valid username and Empty pswd
    Abjurephysicianqa@mailinator.com    ${EMPTY}



*** Keywords ***
Both Invalid
    [Arguments]    ${Username}    ${Password}
    Login    ${Username}    ${Password}  
    Click on Login
    Wait Until Element Contains    //div[@class='ant-notification-notice-message']    Hospital details not found with the given data
    Sleep    2

Both Empty
    [Arguments]    ${Username}    ${Password}
    Login    ${Username}    ${Password}  
    Click on Login
    Element Should Contain    //*[@id="root"]/div/div[2]/div/div/form/div[1]/div[2]/div[2]/div    please enter email!
    Element Should Contain    //*[@id="root"]/div/div[2]/div/div/form/div[2]/div[2]/div[2]/div    please enter password!

Empty username and Valid pswd
    [Arguments]    ${Username}    ${Password}
    Login    ${Username}    ${Password}  
    Click on Login
    Element Should Contain    //*[@id="root"]/div/div[2]/div/div/form/div[1]/div[2]/div[2]/div    please enter email!


Valid username and Empty pswd
        [Arguments]    ${Username}    ${Password}
    Login    ${Username}    ${Password}  
    Click on Login
    Element Should Contain    //*[@id="root"]/div/div[2]/div/div/form/div[2]/div[2]/div[2]/div    please enter password!